
// 1. Якщо об'єкт B є прототипом об'єкта A, то всякий раз, коли у B є властивість, наприклад колір, A  успадкує той же самий колір, якщо інше не вказано явно. І нам не потрібно повторювати всю інформацію про A, яку він успадковує від  B.

// 2. У конструкторі ключове слово super() використовується як функція, що викликає батьківський конструктор з усіма властивостями


class Employee{
    constructor(name, age, salary){
       this._name = name,
       this._age = age,
       this._salary = salary
    }
    greet(){
         console.log('Hello ' + this._name +'. Your salary in ' + this._age + ' is ' + this._salary + ' dollars');
    } 
    
    get name(){
        console.log('This employee has name ' + this._name);
        return this._name
    }

    set name(fullName){
        this._name = fullName
    }

    get age(){
        console.log('This employee is ' + this._age + ' years old');
        return this._age
    }

    set age (newAge){
      this.age = newAge
    }

    get salary(){
        console.log(this._salary + ' is enough to live in the capital');
        return this._salary
    }

    set salary (newSalary) {
        this._salary = newSalary
    }

    
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }
    get lang(){
        console.log(`This employee knows the language ${this._lang}`);
        return this._lang
    }
    set lang(newLang){
        this._lang = newLang
    }

    get salary(){
        return this._salary *3
    }
}



const vasya = new Programmer('Vasya', 70, 1200, 'JS')
console.log(vasya);

console.log('------- Vasya ---------');


console.log(vasya.name);
console.log(vasya.age);
console.log(vasya.salary);
console.log(vasya.lang);

vasya.greet()

console.log('------- John ---------');

const john = new Programmer('John', 45, 4500, 'ua')
console.log(john.name);
console.log(john.age);
console.log(john.salary);
console.log(john.lang);

john.greet()
